![Visitor Badge](https://visitor-badge.laobi.icu/badge?page_id=UnityLxthel&title=Visitors) ![Followers Badge](https://img.shields.io/github/followers/UnityLxthel?label=Followers)
# Hi there 👋

<div style="text-align: center;">
  I'm Unity, I am a discord bot developer.<br/>
  For the most part, I prefer to code in JS.
</div>
<br/>

[![Top Langs](https://github-readme-stats.vercel.app/api/top-langs/?username=UnityLxthel&layout=compact)](https://github.com/UnityLxthel/)
![Unity's github stats](https://github-readme-stats.vercel.app/api?username=UnityLxthel&hide=issues)
--------
<div style="text-align: center;">
  Check out my cool projects!

  [FruityBot](https://github.com/UnityLxthel/FruityBot) a nice discord bot.
</div>
</br>
